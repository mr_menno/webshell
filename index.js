const express = require('express');
const bodyParser = require('body-parser');
const { spawn } = require('child_process');

const authtoken=Buffer.from(new Date().toISOString()).toString('base64');
console.log("***Authentication Token***");
console.log("username: webshell");
console.log("password: "+authtoken);

const PORT=process.env.PORT||3000;

let app = new express();

app.use(express.static(__dirname+'/client'))

app.use(bodyParser.json());

app.post('/terminal-rpc',(req,res) => {

  if(req.body.method==='system.describe') {
    return res.json({
      jsonrpc:'2.0',
      id:req.body.id,
      result:{
        "sdversion": "1.0",
        "name": "DemoService",
        "address": "http:\/\/example.com\/rpc",
        "id":"urn:md5:4e39d82b5acc6b5cc1e7a41b091f6590",
        "procs" :[
          {"name":"echo","params":["string"]}
        ]
      }
    })
  } else if(req.body.method==='login') {
    try {
      if(req.body.params[0]==="webshell" && req.body.params[1]===authtoken) {
        return res.json({
          jsonrpc:'2.0',
          id:req.body.id,
          result: authtoken
        });
      } else {
        return res.json({
          jsonrpc:'2.0',
          id:req.body.id,
          error: {code: 401, message: "invalid authorization"}
        });
      }
    } catch(e) {
      return res.json({
        jsonrpc:'2.0',
        id:req.body.id,
        error: {code: 401, message: "invalid authorization"}
      });
    }
  }

  //test for authentication
  try {
    let _authtoken = req.body.params.shift();
    if(_authtoken!==authtoken) {
      return res.json({jsonrpc:'2.0',error:{code:401, message: "invalid authentication - please logout (type: exit)"},id:req.body.id});
    }
  } catch(e) {
    return res.json({jsonrpc:'2.0',error:{code:401, message: "invalid authentication - please logout (type: exit)"},id:req.body.id});
  }

  if(req.body.method==='destroy') {
    setTimeout(() => process.exit(),1000);
    return res.json({jsonrpc:'2.0',result:">>>> SHUTTING DOWN <<<<",id:req.body.id});
  }

  let _execute = spawn(req.body.method,req.body.params, { 
    maxBuffer: 1024 * 500,
    timeout: 10000,
    shell: true
  });

  let _result = "";

  _execute.stdout.on('data', (data) => {
    _result += "\n"+data;
  })
  _execute.stderr.on('data', (data) => {
    _result += "\n"+data;
  })
  _execute.on('close', (code) => {
    _result += "\n>>> process exited with code: "+code;
    res.json({jsonrpc:'2.0',result:_result,id:req.body.id});
  })
  _execute.on('error', (err) => {
    console.log(err);
    res.json({jsonrpc:'2.0', error: {code:1,message: err.message}, id: req.body.id})
  })

})

app.listen(PORT,() => {
  console.log('>>> listening on '+PORT);
})

process.on('SIGINT', function() {
  process.exit();
});