FROM ubuntu:20.04
RUN apt-get update -y && \
    apt-get install -y curl wget xz-utils
RUN mkdir /opt/node && cd /opt/node && \
    wget https://nodejs.org/dist/v16.13.2/node-v16.13.2-linux-x64.tar.xz && \ 
    tar -xf node-v16.13.2-linux-x64.tar.xz && \ 
    export PATH=$PATH:/opt/node/node-v16.13.2-linux-x64/bin && \
    npm install -g yarn && \
    ln -s /opt/node/node-v16.13.2-linux-x64/bin/* /usr/bin
COPY . /app
RUN cd /app && yarn install
CMD ["node","/app/index.js"]